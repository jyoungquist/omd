\section{Combinatorial Content}
\label{sec:combinatorial}


\subsection{2-Tree Modification}
\label{sec:combinatorial:2tree}

In this section we detail a method in which to modify an input graph into a \term{2-tree}. A \term{$k$-tree} is defined constructively as any graph that can be formed from a complete graph on $k$ vertices and zero or more of the following operation: add a vertex and make it adjacent to all of the nodes of a $k$-clique in the graph. A graph $G=(V,E)$ is a \term{partial $k$-tree} if there exists some set of edges $F$ such that $G'=(V,E\cup F)$ is a $k$-tree. Partial 2-trees are also called treewidth 2, series-parallel, and $K_4$-minor-free graphs.

A graph is a \term{minor} of another graph if it can be obtained by edge contractions, edge deletions, and vertex deletions. An \term{edge contraction} of vertex $s$ into $t$ is: (1) for all edges $(s,v)\in E$, delete $(s,v)$ and add $(t,v)$ to $E$ and (2) delete $s$ from $V$.

The $K_4$-minor-free characterization of partial 2-trees is an important one that has motivated the design of efficient algorithms for recognizing partial 2-trees \todo{Citation}. We also use it here to find a \term{spanning} partial 2-tree. Finding the largest such subgraph is known to be NP-complete \todo{Citation}, however we present a method to find one that is maximal.

We call the following operation a \term{2-contraction}: (1) delete all vertices of degree 0 and 1 and (2) repeatedly contract any vertex of degree 2 into either neighbor.

\begin{lemma}
\label{lem:2contraction_vertex_is_p2tree}
    A graph is a partial 2-tree if and only if its 2-contraction is a single vertex.
\end{lemma}

\begin{proof}
    Follows from the $K_4$-minor-free characterization of partial 2-trees. A 2-contraction is a minor of the input. Furthermore, if a graph has only vertices of degree 3 and greater, then there is some way to delete edges and vertices to find a $K_4$ minor.
\end{proof}

% \begin{proof}
%     For the forward direction, if the 2-contraction results in a single vertex, then we can direct the edges of the contraction in the original graph as such: if $s$ is contracted into $t$ then direct the edge from $s$ to $t$ and if there is a duplicate edge as a result of the contraction then direct that edge in the same direction. As you repeatedly contract you get edges that weren't in the original graph, however, upon contracting these, simply direct the edge that it corresponds to in the original graph. This assignment of directions in the original graph will have exactly one source and sink, and is thus series parallel.
% \end{proof}

Upon contracting an edge in $G=(V,E)$ to get $G'=(V',E')$, we can make an injective \todo{one-to-one} mapping $c:E'\to E$ that we call the \term{corresponding edge}. If $s$ is contracted into $t$, then for all new edges $(t,v)\in E'$, $c((t,v))=(s,v)$ and for all old edges $(u,v)\in E,E'$, $c((u,v))=(u,v)$. When considering 2-contractions, several edge contractions are used giving a sequence of maps $(c_1, c_2, \ldots, c_n)$. We can abuse the terminology and call the edge $c(e)\equiv c_1(c_2(\ldots(c_n(e))))$ the corresponding edge of $e$.

% If the 2-contraction does not result in a single vertex, by removing any edge $e$ and reapplying 2-contraction, if we get a single vertex then removing an edge in the original graph whose corresponding edge is $e$, then the resulting graph is a partial 2-tree.


\begin{lemma}
\label{lem:2contraction_minus_edge}
    Given graph $G=(V,E)$, take $G'=(V',E')$ to be the 2-contraction of $G$ and $c$ to be the corresponding edge map. Take an edge set $F'\subset E'$ and the edge set $F=\{f | \exists f'\in F' [ f=c(f')] \}$ \todo{okay to say $F=c(F')$? seems pretty clear}. The 2-contraction of $(V',E'\setminus F')$ is isomorphic to the 2-contraction of $(V,E\setminus F)$.
\end{lemma}

\begin{proof}
    Edge deletions and the operations of 2-contraction are all valid operations for finding minors. The order in which minor operations are applied is irrelevant, they find the same minor. \todo{How do I say that this is common knowledge?}
\end{proof}

% \begin{lemma}
%     Given graph $G=(V,E)$ with a 2-contraction $G'=(V',E')$ and corresponding edge map $c$ where $|E|>0$, take an edge $e'\in E'$. If the 2-contraction of $H=(V,E\setminus \{c(e')\})$ is a single vertex, then $H$ is a partial 2-tree.
% \end{lemma}

Consider the following algorithm \textsc{MaximalSpanningPartial2Tree} which takes as input a graph $G=(V,E)$ and returns a set of edges $D$: (1) Compute $G'=(V',E')$ and $c$, the 2-contraction of $G$ and the corresponding edge map. (2) If $|E'|=0$, terminate and return $D$. (3) Delete arbitrary edge $e'\in E'$ and add $c(e')$ to $D$. (4) Set $G$ to $G'$ and go to step 1.

\begin{corollary}
    Given graph $G=(V,E)$ and edge set $D$ from running \textsc{MaximalSpanningPartial2Tree} on $G$, the graph $H=(V,E\setminus D)$ is a maximal spanning partial 2-tree of $G$.
\end{corollary}

\begin{proof}
    That $H$ is a partial 2-tree follows from Lemmas~\ref{lem:2contraction_vertex_is_p2tree} and~\ref{lem:2contraction_minus_edge} by induction. \todo{Basic argument, do I need to write it out?} $H$ is spanning because no vertices are removed. $H$ is maximal because the order in which edges are removed has no impact on the resulting 2-contraction, i.e. the results are isomorphic (follows from Lemma~\ref{lem:2contraction_minus_edge}). Therefore, there could be no $D'\subsetneq D$ such that the 2-contraction of $(V,E\setminus D')$ is also a single vertex.

    \todo{Church-Rosser property...}
\end{proof}

\begin{proof}
    \todo{prove}
    Base case: $D$ is empty, $G$ is already a partial 2-tree and $H=G$.
\end{proof}

See Figure \todo{$K_{3,3}$} for a demonstration of this algorithm on the $K_{3,3}$. As mentioned above, finding a maximum spanning partial 2-tree is NP-complete, so it is not surprising that the output of this algorithm can be arbitrarily bad. Consider the graph in Figure \todo{make figure of a bunch of bubbles and the long edge}. This shows two possible sets of edges to drop that the algorithm could return, the optimal set of one from the big arc and the linear worst case choice of one from each bubble.

The next algorithm we introduce is \textsc{CompletePartial2Tree} which takes as input a graph $G=(V,E)$ which is a partial 2-tree and returns a sequence of edges $A$: (1) While $V\neq \emptyset$, select $v\in V$, the vertex of lowest degree. (2) If the degree of $v$ is 0, select arbitrary $(u,w)\in E$ and append the edges $(v,u)$ and $(v,w)$ to $A$ and delete $v$. (3) If the degree of $v$ is 1, select $u$ an arbitrary neighbor of the neighbor of $v$ and append edge $(v,u)$ to $A$ and delete $v$. (4) If the degree of $v$ is 2 and (4a) if the two neighbors of $v$ share an edge, delete $v$; (4b) else, add an edge between the neighbors of $v$ to $E$ and append it to $A$, delete $v$. (5) Else, terminate and return $A$.

\begin{lemma}
    Given partial 2-tree $G=(V,E)$ and edge sequence $A$ from running \textsc{CompletePartial2Tree} on $G$, the graph $H=(V,E\cup A)$ is a 2-tree.
\end{lemma}

\begin{proof}
    \todo{Prove}
    Base case: A partial 2-tree
\end{proof}

Furthermore, this algorithm establishes a partial order on the added edges.

The order in which the algorithm adds these edges establishes a partial order that corresponds to the order in which OMD must solve. The edge length intervals of these edges can be determined if (not only if) all preceding edge lengths are constant.

\begin{lemma}
    Given partial 2-tree $G=(V,E)$ and edge sequence $A=(a_1, a_2, \ldots, a_n)$ from running \textsc{CompletePartial2Tree} on $G$, the edge length interval of $a_i$ can be determined if the lengths of $a_1, a_2, \ldots, a_{i-1}$ are constant.
\end{lemma}

\begin{proof}
    \todo{Prove}
\end{proof}

% ....


% \todo{Does this imply there is some kind of matroid? Those edges that can be deleted are somehow dependent on the lobes that are made when it's removed? Based on the choice of edge for contraction (a random selection among adjacent edges) you could end up deleting different edges. Does this mean we can find a minimum?}


\subsection{3-Tree Modification}
\label{sec:combinatorial:3tree}

In this section, we generalize the concepts of the previous section to modify graphs into 3-trees. We call the following operation a \term{3-contraction}: (1)



\subsection{Realization through Recombination}
\label{sec:combinatorial:recomb}
