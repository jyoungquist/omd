#Constraint Solver

##Installation
Tested on Ubuntu.

1. Install all dependencies.
    - Python 2.7
    - Cython
    - CMake (Makes things much easier)
    - Boost (specifically boost_graph)
2. Download the repository.
3. Compile by navigating to src directory and perform the following:
    `mkdir build && cd build`
    `cmake ..`
    `make pygui`
4. pygui will generate a python based gui to use, other target is searcher which generates a wrapper main
5. Test with `python PyGUI/Constraint_Searcher.py`.

##Usage
This software recognizes dot files with vertex properties `x` and `y` and edge property `length`. All graphs are interpreted as 2D bar-joint graphs.

Sample dot files are available in the `test_files` directory.