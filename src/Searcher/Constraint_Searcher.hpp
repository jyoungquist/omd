#ifndef RECOMBINATION_HPP
#define RECOMBINATION_HPP

#include "../Graph/Graph.hpp"
#include "../Graph/Isostatic_Graph_Realizer.hpp"
#include "spline.hpp"
#include <vector>
#include <cmath>
#include <cstdio>

typedef Oriented_Framework::orientation_types orientation_t;

namespace math_tools{
    double func_distance(spline&, spline&, double, double, int slices=50);
    double lerp(double, double, double, double);
    double lerp(double, double, double);
}

class Constraint_Curve{

public:
    class Curve_Piece{
    public:
        Curve_Piece(unsigned i) : start(i){}
        ~Curve_Piece(){
            std::cout << "\n\n\n\n\n\nDYING CURVE " << this << "\n\n\n\n\n\n" << std::endl;
            for(double * d : lower_crit_params)
                delete [] d;

            if(old_lowers != NULL)
                delete [] old_lowers;
        }
        std::vector<double> y;
        std::vector<double> x;
        std::vector<double> crit_values;
        std::vector<double*> lower_crit_params;

        double * new_lowers;
        double * old_lowers = NULL;
        
        spline spl;
        
        double distance(Curve_Piece & cp);

        int sign = 0;

        unsigned start;
        unsigned end; 

        unsigned id = 0;
    };

    typedef Curve_Piece * Curve_ID;

    std::vector<Curve_ID> constraint_values;

    std::map<unsigned, Curve_ID> mani_ids;
    
    std::vector<double> param_vals;
    Curve_ID current;
    double constraint_val;
    int start_crit = -1;
    int end_crit = -1;

    Constraint_Curve(){constraint_val = 0;}
    Constraint_Curve(double val) : constraint_val(val) {}
    ~Constraint_Curve(){
        for(Curve_Piece * c : constraint_values)
            delete c;
    }

    Curve_ID start_piece(unsigned start){
        constraint_values.push_back(new Curve_Piece(start));
        this->current = constraint_values[constraint_values.size()-1];
        std::cout << "CPIECE INSERTED " << current << std::endl;
        return current;
    }

    void switch_curve(Curve_ID c_id){
        this->current = c_id;
    }

    void end_piece(Curve_ID piece, unsigned end){
        piece->end = end;
    }

    std::vector<double> ys(int idx);
    std::vector<std::vector<double> * > ys();
    std::vector<double> xs(){return param_vals;}
    std::vector<double> xs(int idx);
    std::vector<double> critical_values(int idx);
    std::vector<double> all_critical_values();
    std::vector<std::vector<double> * >  critical_values();
    std::vector<std::vector<double> * >  non_empty_critical_values();
    std::vector<std::vector<double*> *>  lower_criticals();
    std::vector<std::vector<double*> *>  non_empty_lower_criticals();
    std::vector<Curve_ID>  non_empty_ids();
    double constraint(){return constraint_val;}
    int number_of_curves(){return constraint_values.size();}
};

class Manifold_Piece{

public:
    unsigned dim;
    std::vector<Constraint_Curve *> c_curve;
    std::vector<Manifold_Piece *> pieces;

    static unsigned NEXT_CURVE_ID;

    static unsigned curve_id(){return NEXT_CURVE_ID++;};

    Manifold_Piece * parent;

    Manifold_Piece(){
        this->parent = NULL;
        dim = 0;
    }

    Manifold_Piece(Manifold_Piece * parent){
        this->parent = parent;
        dim = this->parent->dim+1;
    }

    ~Manifold_Piece(){
        std::cout << "\n\n\n\n\nDYING MANIFOLD\n" << dim << "\n\n\n\n\n" << std::endl;
        for(int i=0; i<c_curve.size(); i++)
            delete c_curve[i];

        for(int i=0; i<pieces.size(); i++)
            delete pieces[i];
    }

    Manifold_Piece * insert_manifold(){
        Manifold_Piece * mf = new Manifold_Piece(this);
        pieces.push_back(mf);
        return mf;
    }
    Constraint_Curve * insert_curve(double length){
        Constraint_Curve * cc = new Constraint_Curve(length);

        c_curve.push_back(cc);

        return cc;
    }

};

class Constraint_Searcher{

    private:
    struct Constraint;

    struct Parameter{
        Edge_ID e_id;
        std::pair<double, double> interval;
        std::pair<double, double> crit_interval;
        int current_step;
        int uniform_steps;
        int binary_steps;
        Constraint * paired_constraint;

        Parameter(Edge_ID e, std::pair<double, double>& interval, int uniform, int binary, Constraint * constraint = NULL){
            e_id = e;
            this->interval.first = interval.first;
            this->interval.second = interval.second;
            this->crit_interval.first = interval.first;
            this->crit_interval.second = interval.second;
            uniform_steps = uniform;
            binary_steps = binary;
            current_step = 0;
            paired_constraint = constraint;
        }
    };

    struct Constraint{
        std::pair<Vertex_ID, Vertex_ID> edge;
        double distance;

        Constraint(std::pair<Vertex_ID, Vertex_ID> edge, double d){
            this->edge = edge;
            distance = d;
        }
    };

    public: typedef std::vector<Parameter>::iterator Parameter_Iterartor;
            typedef std::vector<Constraint>::iterator Constraint_Iterartor;


    private:

    std::vector<Edge_ID> parameter_ids;

    std::vector<std::pair<Vertex_ID, Vertex_ID> > constraint_edges; 
    std::vector<float> constraint_values;

    std::vector<Parameter> parameters;
    std::vector<Constraint> constraints;

    std::vector<Manifold_Piece> manifold_peices;
    Manifold_Piece * curr_mani = NULL; 
    Manifold_Piece * top_mani = NULL;   
    double * current_lower_params;
    double * previous_lower_params;

    int binary_fail = 0;

    int current_depth = 0;

    Graph * graph;
    std::vector<int> UNIFORM_STEPS;
    std::vector<int> BINARY_STEPS;
    double EPSILON = 0.0005f;

    std::vector<Isostatic_Graph_Realizer::realization_triplet> realization_order;
    std::vector<orientation_t> _orientation;

    double _binary_search(Parameter_Iterartor,double,double,double*,double*,int);
    double fine_search(Parameter_Iterartor,int);
    int search_with_edge_set(Parameter_Iterartor);
    int build_manifold_piece(Parameter_Iterartor);
    int build_manifold_simple(Parameter_Iterartor);
    int build_manifold_complicated(Parameter_Iterartor);
    void find_crit_curve(Manifold_Piece *, int&, int&);
    Constraint_Curve * get_lower_cc(Parameter_Iterartor, int);
    int realize_and_fine_search(Parameter_Iterartor, Constraint &, Constraint_Curve *, Constraint_Curve::Curve_ID, int&);
    void match_curves();

    void order_single_curve(
        std::vector<double> * big, std::vector<double> * little,
        std::vector<int> & order, std::vector<int> & missing_order,
        std::vector<Constraint_Curve::Curve_ID> & curve, int);

    void order_all_curves(
        Constraint_Curve * cc,
        std::vector<std::vector<double> * > & more_curves,
        std::vector<std::vector<double> * > & less_curves,
        std::vector<int> & curves_order, std::vector<int> & curves_missing_order,
        int curves_diff, std::vector<std::vector<int> *> & orders,
        std::vector<std::vector<int> *> & missing_orders,
        std::vector<int> & needs_deleting, 
        std::vector<std::vector<Constraint_Curve::Curve_ID> * > & curves);

    int search_single_curve(
        Parameter_Iterartor p_it, Constraint_Curve * cc, double weight,
        std::vector<double> * big, std::vector<double> * little,
        std::vector<double*> * big_vals, std::vector<double*> * little_vals,
        std::vector<int> & order, std::vector<int> & missing_order,
        std::vector<Constraint_Curve::Curve_ID> & curve, int);

    public:
    static const int DEFAULT_UNIFORM_STEPS;
    static const int DEFAULT_BINARY_STEPS;

    Constraint_Searcher(Graph * g);
    Constraint_Searcher(const char *);
    Constraint_Searcher(Graph * g, 
        std::vector<Edge_ID> & parameters, 
        std::vector<std::pair<Vertex_ID, Vertex_ID> > & constraints, 
        std::vector<float> & distances);
    ~Constraint_Searcher();

    int search();

    void init_sampling();

    Constraint_Curve * get_Constraint_Curve(std::vector<int>);
    void epsilon(float);
    void uniform(int);
    void uniform(std::vector<int> &);
    void binary(int);
    void binary(std::vector<int> &);
    void orientation(std::vector<int> &);
    std::vector<int> orientation();

    std::vector<int> & binary();
    std::vector<int> & uniform();
    double epsilon();


    std::string graph_to_string(){
        return graph->write_to_string();
    }

    void solve_graph();
    void set_params(std::vector<double>);

    std::pair<double, double> get_param_interval(int i){
        return parameters[i].interval;
    }
};

#endif
