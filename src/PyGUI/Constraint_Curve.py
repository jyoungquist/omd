import os,sys,inspect
currentdir = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0,parentdir)

os.environ["ETS_TOOLKIT"] = "qt4"


import numpy as np
from PyQt4 import QtGui
from QT_GUI import Constraint_Window
import settings
import matplotlib.pyplot as plt
import matplotlib.animation as ani
import scipy.interpolate as sp
from mpl_toolkits.mplot3d import axes3d

import pysearch as ps

if __name__ == '__main__':
    if len(sys.argv) > 1:
        cs = ps.PySearcher(sys.argv[1])
    else:
        cs = ps.PySearcher("../test_files/k33_constraints.dot")

    settings.init()

    cs.binary(settings.binary)
    cs.uniform(settings.uniform)
    app = QtGui.QApplication(sys.argv)

    main = Constraint_Window(cs)
    main.show()

    sys.exit(app.exec_())

