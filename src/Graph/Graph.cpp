#include "Graph.hpp"

#include <boost/graph/copy.hpp>

#include <boost/config.hpp>
#include <boost/graph/graph_utility.hpp>
#include <boost/graph/simple_point.hpp>
#include <boost/property_map/property_map.hpp>

#include <boost/graph/circle_layout.hpp>
#include <boost/graph/kamada_kawai_spring_layout.hpp>

#include <boost/graph/random_layout.hpp>
#include <boost/graph/fruchterman_reingold.hpp>

#include <boost/graph/graphviz.hpp>

#include <boost/graph/iteration_macros.hpp>

#include <iostream>
#include <fstream>
#include <map>
#include <vector>

Vertex_ID Graph::add_vertex(Vertex_Properties vp) {
    return this->Graph_Type::add_vertex(vp);
}

Vertex_ID Graph::add_vertex() {
    return this->add_vertex(Vertex_Properties());
}

Edge_ID Graph::add_edge(Vertex_ID v0, Vertex_ID v1, Edge_Properties ep) {
    std::pair<Edge_ID, bool> p = this->Graph_Type::add_edge(v0, v1, ep);
    Edge_ID e = p.first;
    return e;
}

Edge_ID Graph::add_edge(Vertex_ID v0, Vertex_ID v1, double distance) {
    return this->add_edge(v0, v1, Edge_Properties(distance));
}

void Graph::remove_vertex(Vertex_ID v) {
    std::vector<Vertex_ID> vs;

    for (std::pair<Graph_Adj_Iterator, Graph_Adj_Iterator> v_adj = boost::adjacent_vertices(v, *this);
        v_adj.first != v_adj.second;
        v_adj.first++)
    {
        vs.push_back(*(v_adj.first));
    }

    for (std::vector<Vertex_ID>::iterator v_it = vs.begin(); v_it != vs.end(); v_it++) {
        remove_edge(v, *v_it);
    }
   boost::remove_vertex(v, *this);
}

void Graph::remove_edge(Edge_ID e) {
    boost::remove_edge(e, *this);
}

void Graph::remove_edge(Vertex_ID v0, Vertex_ID v1) {
    boost::remove_edge(v0, v1, *this);
}


bool Graph::has_edge(Vertex_ID v0, Vertex_ID v1) const {
    return boost::edge(v0, v1, *this).second;
}

Edge_ID Graph::edge(Vertex_ID v0, Vertex_ID v1) const {
    return boost::edge(v0, v1, *this).first;
}

void Graph::contract_edge(Vertex_ID v0, Vertex_ID v1) {
   std::pair<Graph_Adj_Iterator, Graph_Adj_Iterator> v0_adj = boost::adjacent_vertices(v0, *this);
   for (Graph_Adj_Iterator v0_adj_it = v0_adj.first; v0_adj_it != v0_adj.second; ++v0_adj_it) {
        // don't add any edges from v1 to v1
        // only add edge if it's not already in
        if (v1 != *v0_adj_it && !has_edge(v1, *v0_adj_it))
            boost::add_edge(v1, *v0_adj_it, *this);
    }
    boost::clear_vertex(v0, *this);
    remove_vertex(v0);

std::pair<Vertex_Iterator, Vertex_Iterator> Graph::vertices() const {
    return boost::vertices(*this);
}

std::pair<Edge_Iterator, Edge_Iterator> Graph::edges() const {
    return boost::edges(*this);
}

unsigned int Graph::degree_of_vertex(Vertex_ID v) const {
    return boost::degree(v, *this);
}


std::set<Vertex_ID> Graph::vertices_adjacent(Vertex_ID v) const {
    std::set<Vertex_ID> ret;

    boost::graph_traits<Graph>::adjacency_iterator v_it, v_end;
    for (boost::tie(v_it, v_end) = boost::adjacent_vertices(v, *this);
        v_it != v_end; v_it++)
    {
        ret.insert(*v_it);
    }

    return ret;
}

std::set<Vertex_ID> Graph::vertices_adjacent(std::set<Vertex_ID> &v_set) const {
    std::set<Vertex_ID> ret;
    for (std::set<Vertex_ID>::iterator set_v = v_set.begin(); set_v != v_set.end(); set_v++) {
        boost::graph_traits<Graph>::adjacency_iterator v, v_end;
        for (boost::tie(v, v_end) = boost::adjacent_vertices(*set_v, *this);
            v != v_end; v++)
        {
            ret.insert(*v);
        }
    }

    return ret;
}

std::pair<Vertex_ID, Vertex_ID> Graph::vertices_incident(Edge_ID e) const {
    return std::make_pair(boost::source(e, *this), boost::target(e, *this));
}

std::set<Edge_ID> Graph::edges_incident(Vertex_ID v) const {
    std::set<Edge_ID> ret;
    std::pair<in_edge_iterator, in_edge_iterator> es = boost::in_edges(v, *this);
    for (in_edge_iterator e_it = es.first; e_it != es.second; e_it++) {
        ret.insert(*e_it);
    }
    return ret;
}

std::set<Edge_ID> Graph::edges_incident(std::set<Vertex_ID> vs) const {
    std::set<Edge_ID> ret;
    for (std::set<Vertex_ID>::iterator v_it = vs.begin(); v_it != vs.end(); v_it++) {
        std::pair<in_edge_iterator, in_edge_iterator> es = boost::in_edges(*v_it, *this);
        for (in_edge_iterator e_it = es.first; e_it != es.second; e_it++) {
            ret.insert(*e_it);
        }
    }
    return ret;
}

// this!!!
std::set<Edge_ID> Graph::edges_in_induced_subgraph(std::set<Vertex_ID> vs) const {
    std::set<Edge_ID> ret;
    for (std::set<Vertex_ID>::iterator v_it = vs.begin(); v_it != vs.end(); v_it++) {
        boost::graph_traits<Graph>::adjacency_iterator v_adj_it, v_end;
        for (boost::tie(v_adj_it, v_end) = boost::adjacent_vertices(*v_it, *this);
            v_adj_it != v_end; v_adj_it++)
        {
            if (vs.find(*v_adj_it) != vs.end()) {
                Edge_ID e = boost::edge(*v_it, *v_adj_it, *this).first;
                ret.insert(e);
            }
        }
    }
    return ret;
}

// assumes they're disjoint
std::set<Edge_ID> Graph::edges_between(
    std::set<Vertex_ID> &v_set_1,
    std::set<Vertex_ID> &v_set_2) const
{
    std::set<Edge_ID> ret;
    for (std::set<Vertex_ID>::iterator set_v = v_set_1.begin(); set_v != v_set_1.end(); set_v++) {
        std::set<Vertex_ID> adjacent_vs;
        boost::graph_traits<Graph>::adjacency_iterator v, v_end;
        for (boost::tie(v, v_end) = boost::adjacent_vertices(*set_v, *this);
            v != v_end; v++)
        {
            if (v_set_2.find(*v) != v_set_2.end()) {
                ret.insert(edge(*v, *set_v));
            }
        }
    }

    return ret;
}

Vertex_Iterator Graph::find_vertex(const char *name) const {
    Vertex_Iterator v, v_end;
    for (boost::tie(v, v_end) = this->vertices(); v != v_end; v++) {
        if ((*this)[*v].name.compare(name) == 0) {
            return v;
        }
    }

    return v_end;
}


unsigned int Graph::num_vertices() const {
    return boost::num_vertices(*this);
}

unsigned int Graph::num_edges() const {
    return boost::num_edges(*this);
}


void Graph::set_layout() {
    typedef boost::property_map<Graph_Type, Point Vertex_Properties::*>::type Vertex_Point_Map;
    typedef boost::property_map<Graph_Type, double Edge_Properties::*>::type Edge_Distance_Map;

    Vertex_Point_Map vp_map = boost::get(&Vertex_Properties::point, *this);
    Edge_Distance_Map ed_map = boost::get(&Edge_Properties::length, *this);

    boost::circle_graph_layout(*this, vp_map, 100);
    boost::kamada_kawai_spring_layout(
        *this,
        vp_map,
        ed_map,
        boost::square_topology<>(),
        boost::side_length<double>(0.4)
        );
    Vertex_Iterator i, end;
    for (boost::tie(i, end) = this->vertices(); i != end; i++) {
        (*this)[*i].x = vp_map[*i][0];
        (*this)[*i].y = vp_map[*i][1];
    }
}


// TODO: Potentially scale edge length
void Graph::get_graph_in_range(float x_min, float x_max, float y_min, float y_max) {
    float
        del_x = x_max - x_min,
        del_y = y_max - y_min;

    Vertex_Iterator v, end;
    boost::tie(v, end) = this->vertices();
    double
        maxx = (*this)[*v].x,
        minx = (*this)[*v].x,
        maxy = (*this)[*v].y,
        miny = (*this)[*v].y;

    for (; v != end; v++) {
        double x = (*this)[*v].x;
        double y = (*this)[*v].y;

        maxx = (x > maxx)? x: maxx;
        maxy = (y > maxy)? y: maxy;

        minx = (x < minx)? x: minx;
        miny = (y < miny)? y: miny;
    }

    double
        centerx = minx + (maxx - minx)/2,
        centery = miny + (maxy - miny)/2;

    maxx -= centerx; minx -= centerx;
    maxy -= centery; miny -= centery;

    double
        diff_x = (maxx > -minx)? maxx: -minx,
        diff_y = (maxy > -miny)? maxy: -miny,
        scale_x = del_x/2/diff_x,
        scale_y = del_y/2/diff_y,
        scale = (scale_x < scale_y)? scale_x: scale_y;

    for (boost::tie(v, end) = this->vertices(); v != end; v++) {
        (*this)[*v].x = ((*this)[*v].x - centerx) * scale;
        (*this)[*v].y = ((*this)[*v].y - centery) * scale;
    }
}


void Graph::write_to_file(const char* filename) {
    boost::dynamic_properties dp;

    dp.property("node_id", boost::get(&Vertex_Properties::name, *this));
    dp.property("x", boost::get(&Vertex_Properties::x, *this));
    dp.property("y", boost::get(&Vertex_Properties::y, *this));
    
    dp.property("length", boost::get(&Edge_Properties::length, *this));
    dp.property("type", boost::get(&Edge_Properties::type, *this));


    std::ofstream f(filename);
    Vertex_Writer<Graph> v(*this);
    Edge_Writer<Graph> e(*this);
    boost::write_graphviz(f, *this, v, e);
}

std::string Graph::write_to_string() {
    boost::dynamic_properties dp;
    dp.property("node_id", boost::get(&Vertex_Properties::name, *this));
    dp.property("x", boost::get(&Vertex_Properties::x, *this));
    dp.property("y", boost::get(&Vertex_Properties::y, *this));
    
    dp.property("length", boost::get(&Edge_Properties::length, *this));
    dp.property("type", boost::get(&Edge_Properties::type, *this));
    std::string temp;

    std::ostringstream s(temp);
    Vertex_Writer<Graph> v(*this);
    Edge_Writer<Graph> e(*this);
    boost::write_graphviz(s, *this, v, e);

    return temp;
}


void Graph::read_from_file(const char* filename) {
    boost::dynamic_properties dp;

    // vertex properties
    dp.property("node_id", boost::get(&Vertex_Properties::name, *this));
    dp.property("x", boost::get(&Vertex_Properties::x, *this));
    dp.property("y", boost::get(&Vertex_Properties::y, *this));

    // edge properties
    dp.property("length", boost::get(&Edge_Properties::length, *this));
    dp.property("type", boost::get(&Edge_Properties::type, *this));

    // read in
    std::ifstream f(filename);
    boost::read_graphviz(f, *this, dp);
}

void Graph::print_vertices(unsigned int indent) const {
    std::string tabs = "";
    for (int i = 0; i < indent; i++) tabs+="\t";

    std::cout << tabs << "Graph vertices:" << std::endl;
    tabs += "\t";
    for (std::pair<Vertex_Iterator, Vertex_Iterator> vs = vertices(); vs.first != vs.second; vs.first++) {
        Vertex_ID v = *(vs.first);
        std::cout << tabs
            << "ID: " << v << " | "
            << "Name: " << (*this)[v].name  << " | "
            << "Degree: " << degree_of_vertex(v) << " | "
            << "Position: (" << (*this)[v].x << ", " << (*this)[v].y << ")"
            << std::endl;
    }
}


void Graph::print_edges(unsigned int indent) const {
    std::string tabs = "";
    for (int i = 0; i < indent; i++) tabs+="\t";

    std::cout << tabs << "Graph edges:" << std::endl;
    tabs += "\t";
    for (std::pair<Edge_Iterator, Edge_Iterator> es = edges(); es.first != es.second; es.first++) {
        Edge_ID e = *(es.first);
        std::pair<Vertex_ID, Vertex_ID> vs = vertices_incident(e);

        std::cout << tabs
            << "ID: " << e << " | "
            << "Verts: (" << (*this)[vs.first].name << ", " << (*this)[vs.second].name << ") | "
            << "Param: " << (*this)[e].type << " | "
            << "Length: " << (*this)[e].length
            << std::endl;
    }
}

Mapped_Graph_Copy::Mapped_Graph_Copy(const Graph *g) : orig(g) {
    Vertex_Iterator v, v_end;
    for (boost::tie(v, v_end) = orig->vertices(); v != v_end; v++) {
        add_original_vertex(*v);
    }

    Edge_Iterator e, e_end;
    for (boost::tie(e, e_end) = orig->edges(); e != e_end; e++) {
        add_original_edge(*e);
    }
}

Mapped_Graph_Copy::Mapped_Graph_Copy(const Mapped_Graph_Copy &g) : orig(g.orig) {
    Vertex_Iterator v, v_end;
    for (boost::tie(v, v_end) = g.vertices(); v != v_end; v++) {
        Vertex_ID orig_v = g.original_vertex(*v);
        Vertex_ID new_v = add_vertex(g[*v]);
        orig_to_copy[orig_v] = new_v;
        copy_to_orig[new_v] = orig_v;
    }

    Edge_Iterator e, e_end;
    for (boost::tie(e, e_end) = g.edges(); e != e_end; e++) {
        std::pair<Vertex_ID, Vertex_ID> vs = g.vertices_incident(*e);
        add_edge(
            orig_to_copy[g.original_vertex(vs.first)],
            orig_to_copy[g.original_vertex(vs.second)],
            g[*e].length);
    }
}

Mapped_Graph_Copy::Mapped_Graph_Copy(const Graph *g, std::set<Vertex_ID> &vertices) : orig(g) {
    for (std::set<Vertex_ID>::iterator v_it = vertices.begin(); v_it != vertices.end(); v_it++) {
        add_original_vertex(*v_it);
    }

    Edge_Iterator e, e_end;
    for (boost::tie(e, e_end) = orig->edges(); e != e_end; e++) {
        std::pair<Vertex_ID, Vertex_ID> vs = orig->vertices_incident(*e);
        if (vertices.find(vs.first) != vertices.end() && vertices.find(vs.second) != vertices.end())
            add_edge(orig_to_copy[vs.first], orig_to_copy[vs.second], (*orig)[*e].length);
    }
}


void Mapped_Graph_Copy::add_original_vertex(Vertex_ID orig_v) {
    Vertex_ID new_v = add_vertex((*orig)[orig_v]);
    orig_to_copy[orig_v] = new_v;
    copy_to_orig[new_v] = orig_v;
}

void Mapped_Graph_Copy::add_original_edge(Edge_ID orig_e) {
    std::pair<Vertex_ID, Vertex_ID> vs = orig->vertices_incident(orig_e);
    add_edge(orig_to_copy[vs.first], orig_to_copy[vs.second], (*orig)[orig_e].length);
}


Vertex_ID Mapped_Graph_Copy::original_vertex(Vertex_ID v) const {
    return copy_to_orig.at(v);
}

std::set<Vertex_ID> Mapped_Graph_Copy::original_vertices(const std::set<Vertex_ID> &vs) const {
    std::set<Vertex_ID> ret;
    for (std::set<Vertex_ID>::iterator v_it = vs.begin(); v_it != vs.end(); v_it++)
        ret.insert(copy_to_orig.at(*v_it));
    return ret;
}

// adds every edge incident to the current subgraph, and the associated vertices
void Mapped_Graph_Copy::expand() {
    std::set<Vertex_ID> vs_to_add;
    std::set<Edge_ID>   es_to_add;

    std::pair<Vertex_Iterator, Vertex_Iterator> vs = vertices();
    for (Vertex_Iterator sub_v_it = vs.first; sub_v_it != vs.second; sub_v_it++) {
        Vertex_ID v_copy = *sub_v_it;
        Vertex_ID v_orig = original_vertex(v_copy);

        boost::graph_traits<Graph>::adjacency_iterator adj_v_it, v_end;
        for (boost::tie(adj_v_it, v_end) = boost::adjacent_vertices(v_orig, *orig);
            adj_v_it != v_end; adj_v_it++)
        {
            Vertex_ID v_orig_adj = *adj_v_it;
            Edge_ID e_orig = orig->edge(v_orig, v_orig_adj);

            if (orig_to_copy.find(v_orig_adj) == orig_to_copy.end()) {
                vs_to_add.insert(v_orig_adj);
                es_to_add.insert(e_orig);
            } else {
                Vertex_ID v_copy_adj = copy_vertex(v_orig_adj);
                std::pair<Edge_ID, bool> edge_p = boost::edge(v_copy, v_copy_adj, *this);

                if (!edge_p.second) {
                    es_to_add.insert(e_orig);
                }
            }
        }
    }

    for (std::set<Vertex_ID>::iterator v_it = vs_to_add.begin(); v_it != vs_to_add.end(); v_it++) {
        add_original_vertex(*v_it);
    }

    for (std::set<Edge_ID>::iterator e_it = es_to_add.begin(); e_it != es_to_add.end(); e_it++) {
        add_original_edge(*e_it);
    }
}

// input is a set of original vertices. If they are adjacent to any vertices
// in the copy, the vertex and the incident edges will be added into the copy
void Mapped_Graph_Copy::grow_into(std::set<Vertex_ID> orig_vs) {
    std::set<Vertex_ID> vs_to_add;
    std::set<Edge_ID>   es_to_add;

    std::pair<Vertex_Iterator, Vertex_Iterator> vs = vertices();
    for (Vertex_Iterator sub_v_it = vs.first; sub_v_it != vs.second; sub_v_it++) {
        Vertex_ID v_copy = *sub_v_it;
        Vertex_ID v_orig = original_vertex(v_copy);

        boost::graph_traits<Graph>::adjacency_iterator adj_v_it, v_end;
        for (boost::tie(adj_v_it, v_end) = boost::adjacent_vertices(v_orig, *orig);
            adj_v_it != v_end; adj_v_it++)
        {
            Vertex_ID v_orig_adj = *adj_v_it;

            if (orig_vs.find(v_orig_adj) == orig_vs.end())
                continue;

            Edge_ID e_orig = orig->edge(v_orig, v_orig_adj);

            if (orig_to_copy.find(v_orig_adj) == orig_to_copy.end()) {
                vs_to_add.insert(v_orig_adj);
                es_to_add.insert(e_orig);
            } else {
                Vertex_ID v_copy_adj = copy_vertex(v_orig_adj);
                std::pair<Edge_ID, bool> edge_p = boost::edge(v_copy, v_copy_adj, *this);

                if (!edge_p.second) {
                    es_to_add.insert(e_orig);
                }
            }
        }
    }

    for (std::set<Vertex_ID>::iterator v_it = vs_to_add.begin(); v_it != vs_to_add.end(); v_it++) {
        add_original_vertex(*v_it);
    }

    for (std::set<Edge_ID>::iterator e_it = es_to_add.begin(); e_it != es_to_add.end(); e_it++) {
        add_original_edge(*e_it);
    }

}

Vertex_ID Mapped_Graph_Copy::copy_vertex(Vertex_ID v) const {
    return orig_to_copy.at(v);
}

void Mapped_Graph_Copy::print_copy_to_orig(unsigned int indent) {
    for (int i = 0; i < indent; i++) std::cout << "\t";
    std::cout << "copy_to_orig mapping (new, old):" << std::endl;

    for (std::map<Vertex_ID, Vertex_ID>::iterator v = copy_to_orig.begin(); v!= copy_to_orig.end(); v++) {
        for (int i = 0; i < indent+1; i++) std::cout << "\t";
        // std::cout << "(" << v->first << ", " << v->second << ")" << std::endl;
        std::cout << "(" << v->first << ": " << (*this)[v->first].name << ", " << v->second << ": " << (*orig)[v->second].name << ")" << std::endl;
    }
}

Subgraph::Subgraph(const Graph *g) {
    _graph = g;
}

const Graph* Subgraph::graph() const {
    return _graph;
}

void Subgraph::induce(Vertex_Iterator begin, Vertex_Iterator end) {
    for (Vertex_Iterator v_it = begin; v_it != end; v_it++)
        add_vertex(*v_it);
}

void Subgraph::induce(const std::set<Vertex_ID> &vertices) {
    for (std::set<Vertex_ID>::iterator v_it = vertices.begin();
        v_it != vertices.end(); v_it++)
    {
        add_vertex(*v_it);
    }
}

void Subgraph::add_vertex(Vertex_ID vertex) {
    _vertices.insert(vertex);

    boost::graph_traits<Graph>::adjacency_iterator v, v_end;
    for (boost::tie(v, v_end) = boost::adjacent_vertices(vertex, *_graph);
        v != v_end; v++)
    {
        if (_vertices.find(*v) != _vertices.end()) {
            Edge_ID e = boost::edge(vertex, *v, *_graph).first;
            _edges.insert(e);
        }
    }
}

void Subgraph::remove_vertex(Vertex_ID vertex) {
    _vertices.erase(vertex);

    boost::graph_traits<Graph>::adjacency_iterator v, v_end;
    for (boost::tie(v, v_end) = boost::adjacent_vertices(vertex, *_graph);
        v != v_end; v++)
    {
        Edge_ID e = boost::edge(vertex, *v, *_graph).first;
        _edges.erase(e);
    }
}

std::pair<Sg_Vertex_Iterator, Sg_Vertex_Iterator> Subgraph::vertices() const {
    return std::pair<Sg_Vertex_Iterator, Sg_Vertex_Iterator>(_vertices.begin(), _vertices.end());
}

std::pair<Sg_Edge_Iterator, Sg_Edge_Iterator> Subgraph::edges() const {
    return std::pair<Sg_Edge_Iterator, Sg_Edge_Iterator>(_edges.begin(), _edges.end());
}

std::set<Vertex_ID> Subgraph::vertices_adjacent(std::set<Vertex_ID> &v_set) const {
    std::set<Vertex_ID> ret;
    for (std::set<Vertex_ID>::iterator set_v = v_set.begin(); set_v != v_set.end(); set_v++) {
        boost::graph_traits<Graph>::adjacency_iterator v, v_end;
        for (boost::tie(v, v_end) = boost::adjacent_vertices(*set_v, *_graph);
            v != v_end; v++)
        {
            ret.insert(*v);
        }
    }

    return ret;
}

// assumes they're disjoint
std::vector<std::set<Vertex_ID> > Subgraph::edges_between(
        std::set<Vertex_ID> &v_set_1,
        std::set<Vertex_ID> &v_set_2) const
{
    std::vector<std::set<Vertex_ID> > ret;
    for (std::set<Vertex_ID>::iterator set_v = v_set_1.begin(); set_v != v_set_1.end(); set_v++) {
        std::set<Vertex_ID> adjacent_vs;
        boost::graph_traits<Graph>::adjacency_iterator v, v_end;
        for (boost::tie(v, v_end) = boost::adjacent_vertices(*set_v, *_graph);
            v != v_end; v++)
        {
            if (v_set_2.find(*v) != v_set_2.end()) {
                std::set<Vertex_ID> add_to;
                add_to.insert(*v);
                add_to.insert(*set_v);
                ret.push_back(add_to);
            }
        }
    }

    return ret;
}

std::pair<Vertex_ID, Vertex_ID> Subgraph::vertices_incident(Edge_ID e) const {
    return _graph->vertices_incident(e);
}


unsigned int Subgraph::num_vertices() const {
    return _vertices.size();
}

unsigned int Subgraph::num_edges() const {
    return _edges.size();
}


const Vertex_Properties& Subgraph::operator[](Vertex_ID vertex) const {
    return (*_graph)[vertex];
}
